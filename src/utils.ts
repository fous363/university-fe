import axios from "axios";

export const axiosInstance = axios.create({ withCredentials: true });

export default function constainsRole(roles: [ { id: number; name: string; } ], roleName: string): boolean {
    const role = roles.map((role: { id: number, name: string}) => role.name).find((role: string) => role === roleName);
    if (!role) {
        return false;
    }
    return true;
}

export function constainsRoleArray(roles: string[], roleName: string): boolean {
    return roles.some(role => role === roleName);
}
