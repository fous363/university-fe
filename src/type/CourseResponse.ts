export default interface CourseResponse {
    id: number,
    name: string
}