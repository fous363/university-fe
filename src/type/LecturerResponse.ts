export default interface LecturerResponse {
    id: number,
    fullName: string,
    phone?: string,
    birthday?: any,
    email?: string,
    academicDegreeId?: number
}