
export default interface StudentResponse {
    id: number,
    fullName: string,
    phone?: string,
    birthday?: any,
    email?: string
}