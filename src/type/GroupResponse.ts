export default interface GroupResponse {
    id: number,
    name: string,
    responsiblePersonId?: number,
    groupStudents: [{ id: number }]
}