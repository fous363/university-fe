import React, { ErrorInfo } from 'react';
import './ErrorHeader.scss';

// TODO: Need better impl of hiding the component
class ErrorHeader extends React.Component {
    state: {error: Error | null, errorInfo: ErrorInfo | null} = { error: null, errorInfo: null };

    static getDerivedStateFromError(error: any) {
        // Update state so the next render will show the fallback UI.
        return { error: error };
      }

    handleClick() {
        this.setState({ error: null, errorInfo: null });
    }

    render() {
        if (this.state.error) {
            return (
            <div>
                <div className="error">
                <span>{this.state.error?.message}</span>
                <button type="button" className="close-btn" onClick={this.handleClick}>&#x2715;</button>
                </div>
                {this.props.children}
            </div>);
        }
        return this.props.children;
    }
}

export default ErrorHeader;