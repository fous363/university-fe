import React, { useEffect, useState } from 'react';
import './ContentHolder.scss';

// todo: change props type to component class
function ContentHolder(props: any) {
    const [isLoading, setLoading] = useState(props.isContentLoading);
    let className = 'content-continer';
    if (props.position) {
        className += '-' + props.position
    }
    

    useEffect(() => {
        console.log(props.isContentLoading);
        setLoading(props.isContentLoading);
    }, [props.isContentLoading]);

    return(
        <div className={className}>
            { isLoading && 
                <div className="loading-container">
                    <h1>Loading...</h1>
                </div>}
            { !isLoading && props.children }
        </div>
    );
}

export default ContentHolder;