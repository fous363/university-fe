import React from 'react';
import { Link } from 'react-router-dom';
import User from '../../type/User';
import './Navbar.scss';
import constainsRole from '../../utils';

interface PageModule {
    path: string,
    name: string,
    shortName: string,
    role?: string[]
}

function Navbar(props: any) {
    const currentPageShorName: string = props.currentPage;
    const pages: PageModule[] = [
        { path: '/profile', name: 'Profile', shortName: 'profile' }, 
        { path: '/course', name: 'Course', shortName: 'course', role: ['admin'] },
        { path: '/group', name: 'Group', shortName: 'group', role: ['admin'] }
    ];
    const storedUser: any = localStorage.getItem('storedUser'); // todo: think about other solution for localStorage
    if (!storedUser) {
        throw new Error('You are not authenticated');
    }
    const userInfo: User = JSON.parse(storedUser);

    return (
        <div className="navbar-container">
            <ul>
            { pages.map(page => {
                    const isDisplyable: boolean = (page.role === null || page.role === undefined) || 
                    (page.role && page.role.some(role => constainsRole(userInfo.roles, role)));
                    if (isDisplyable) {
                        if (page.shortName === currentPageShorName) {
                            return (<li className="disabled"><span>{page.name}</span></li>);
                        }
                        return (
                        <li><Link to={page.path}>{page.name}</Link></li>
                        );
                    }
                    return null;
                })}
            </ul>
        </div>
    );
}

export default Navbar;