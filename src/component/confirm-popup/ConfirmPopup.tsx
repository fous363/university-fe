import React from 'react';
import './ConfirmPopup.scss';

export default function ConfirmPopup(props: any) {
    const text = props.text;

    const handleConfirm = () => {
        if (props.performHandler) {
            props.performHandler();
        }
        props.closeHandler();
    }

    const handleCancel = () => {
        props.closeHandler();
    }

    return(
        <div className="confirm-modal">
            <div className="modal-content">
                <p>{text}</p>
                <div className="btn-group">
                    <button className="yes-btn" onClick={handleConfirm}>YES</button>
                    <button className="no-btn" onClick={handleCancel}>NO</button>
                </div>
            </div>
        </div>
    );
}