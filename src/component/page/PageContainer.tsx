import React from 'react';
import './PageContainer.scss';

function PageContainer(props: any) {
    

    return (
        <div className={props.isVertical ? 'page-container-vertical' : 'page-container-horizontal'}>
            {props.children}
        </div>
    );
}

export default PageContainer;