import React from 'react';
import './pages/login/LoginPage';
import LoginPage from './pages/login/LoginPage';
import ProfilePage from './pages/profile/ProfilePage';
import { Switch, Route } from 'react-router-dom';
import ErrorHeader from './component/error/ErrorHeader';
import './App.scss'
import RegistrationPage from './pages/registration/RegistrationPage';
import CoursePage from './pages/course/CoursePage';
import GroupPage from './pages/group/GroupPage';

function App() {
  return (
    <ErrorHeader>
    <Switch>
      <Route exact path="/" component={LoginPage}/>
      <Route path="/profile" component={ProfilePage}/>
      <Route path="/registration" component={RegistrationPage}/>
      <Route path="/course" component={CoursePage}/>
      <Route path="/group" component={GroupPage}/>
    </Switch>
    </ErrorHeader>
  );
}

export default App;
