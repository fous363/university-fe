import React, { useEffect, useState } from "react";
import ContentHolder from "../../component/content/ContentHolder";
import PageContainer from "../../component/page/PageContainer";
import { axiosInstance } from "../../utils";
import { ErrorResponse } from "../../type/ErrorResponse";
import User from "../../type/User";
import './RegistrationPage.scss';

function buildFullName(fullNameObject: { firstName: string, middleName: string, lastName: string }): string {
    let fullName = fullNameObject.firstName;
    if (fullNameObject.middleName !== '') {
        fullName += ' ' + fullNameObject.middleName;
    }
    fullName += ' ' + fullNameObject.lastName;
    return fullName;
}

function RegistrationPage() {
    const [isLoading, setLoading] = useState(true);
    const [isStudent, setStudent] = useState(true);
    const [isLecturer, setLecturer] = useState(false);
    const [userInfo, setUserInfo] = useState({ username: '', password: ''});
    const [studentInfo, setStudentInfo] = useState({ fullName: { firstName: '', middleName: '', lastName: '' }, email: '', phone: '', birthday: '', userId: -1});
    const [lecturerInfo, setLecturerInfo] = useState({ ...studentInfo, academicDegreeId: 1});
    const [error, setError] = useState('');
    const [academicDegreeList, setAcademicDegreeList] = useState([ { id: 0, name: '' } ]);

    useEffect(() => {
        if (isLoading) {
            axiosInstance.get('api/academicDegree/').then(rsp => {
                const academicDegreeListRsp: [ { id: number, name: string } ] = rsp.data.content;
                setAcademicDegreeList(academicDegreeListRsp);
                setLoading(false);
            }).catch(err => {
                console.error(err);
                const errorResponse: ErrorResponse = err.response.data;
                if (errorResponse.message !== '') {
                    setError(errorResponse.message);
                } else {
                    setError(errorResponse.error);
                }
            });
            console.log(isLoading);
        }
        setLoading(false);
    }, [isLecturer, isLoading]);

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        switch(event.target.id) {
            case 'username':
                setUserInfo({...userInfo, username: event.target.value});
                break;
            case 'password':
                setUserInfo({...userInfo, password: event.target.value});
                break;
            case 'first-name':
                const firstName: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, fullName: { ...studentInfo.fullName, firstName: firstName}});
                } else {
                    setLecturerInfo({...lecturerInfo, fullName: { ...lecturerInfo.fullName, firstName: firstName}});
                }
                break;
            case 'middle-name':
                const middleName: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, fullName: { ...studentInfo.fullName, middleName: middleName }});
                } else {
                    setLecturerInfo({...lecturerInfo, fullName: {...lecturerInfo.fullName, middleName: middleName }});
                }
                break;
            case 'last-name':
                const lastName: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, fullName: { ...studentInfo.fullName, lastName: lastName }});
                } else {
                    setLecturerInfo({...lecturerInfo, fullName: {...lecturerInfo.fullName, lastName: lastName }});
                }
                break;
            case 'phone':
                const phone: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, phone: phone});
                } else {
                    setLecturerInfo({...lecturerInfo, phone: phone});
                }
                break;
            case 'email':
                const email: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, email: email});
                } else {
                    setLecturerInfo({...lecturerInfo, email: email});
                }
                break;
            case 'birthday':
                const birthday: string = event.target.value;
                if (isStudent) {
                    setStudentInfo({...studentInfo, birthday: birthday});
                } else {
                    setLecturerInfo({...lecturerInfo, birthday: birthday});
                }
                break;
            default:
                break;
        }
    }

    const handleSelectChange = (event: any) => {
        console.log(event);
        setLecturerInfo({...lecturerInfo, academicDegreeId: event.target.value });
    }

    const handleStudentSwitching = (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
        setStudent(true);
        setLecturer(false);
        setStudentInfo(studentInfo);
    }

    const handleLecturerSwitching = (event: React.MouseEvent<HTMLDivElement, globalThis.MouseEvent>) => {
        setStudent(false);
        setLecturer(true);
        setLecturerInfo(lecturerInfo);
    }

    const handleRegistrate = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        if (isStudent) {
            axiosInstance.post('api/user/student', userInfo).then(async (rsp) => {
                try {
                    console.log(rsp);
                    const user: User = rsp.data;
                    const studentRequest = { fullName: buildFullName(studentInfo.fullName), phone: studentInfo.phone, 
                        email: studentInfo.email, birthday: studentInfo.birthday, userId: user.id};
                    await axiosInstance.post('api/student/', studentRequest);
                } catch (err) {
                    console.error(err);
                    const errorResponse: ErrorResponse = err.response.data;
                    if (errorResponse.message !== '') {
                        setError(errorResponse.message);
                    } else {
                        setError(errorResponse.error);
                    }
                }
            });    
        } else if (isLecturer) {
            axiosInstance.post('api/user/lecturer', userInfo).then(async (rsp) => {
                try {
                    console.log(rsp);
                    const user: User = rsp.data;
                    const lecturerRequest = { fullName: buildFullName(lecturerInfo.fullName), phone: lecturerInfo.phone, 
                        email: lecturerInfo.email, birthday: lecturerInfo.birthday, userId: user.id, 
                        academicDegreeId: lecturerInfo.academicDegreeId}
                    await axiosInstance.post('api/lecturer/', lecturerRequest);
                } catch (err) {
                    console.error(err);
                    const errorResponse: ErrorResponse = err.response.data;
                    if (errorResponse.message !== '') {
                        setError(errorResponse.message);
                    } else {
                        setError(errorResponse.error);
                    }
                }
            });  
        }   
    }

    // todo: think about other solution for useEffect
    if (error !== '') {
        throw new Error(error);
    }
    return (
        <PageContainer isVertical={true}>
            <div className="switcher">
                <div className="student-switch-btn" onClick={handleStudentSwitching}><span>Student</span></div>
                <span className="divider"></span>
                <div className="lecturer-switch-btn" onClick={handleLecturerSwitching}><span>Lecturer</span></div>
            </div>
            <ContentHolder position='center' isContentLoading={isLoading}>
                <div className="registration-container">
                    <h1>Registration</h1>
                    <form>
                    <div className="input-block">
                            <label htmlFor="username">Enter username:</label>
                            <input id="username" type="text" onChange={handleInputChange} value={userInfo.username}/>
                        </div>
                        <div className="input-block">
                            <label htmlFor="password">Enter password:</label>
                            <input id="password" type="password" onChange={handleInputChange} value={userInfo.password}/>
                        </div>
                        <div className="input-block">
                            <p>Enter your full name:</p>
                            <div className="full-name-block">
                                <input id="first-name" type="text" placeholder="First name" onChange={handleInputChange} 
                                    value={isStudent ? studentInfo.fullName.firstName : lecturerInfo.fullName.firstName}/>
                                <input id="middle-name" type="text" placeholder="Middle name(optional)" onChange={handleInputChange}
                                    value={isStudent ? studentInfo.fullName.middleName : lecturerInfo.fullName.middleName}/>
                                <input id="last-name" type="text" placeholder="Last name" onChange={handleInputChange}
                                    value={isStudent ? studentInfo.fullName.lastName : lecturerInfo.fullName.lastName}/>
                            </div>
                        </div>
                        <div className="input-block">
                            <label htmlFor="email">Enter email:</label>
                            <input id="email" type="text" onChange={handleInputChange} value={isStudent ? studentInfo.email : lecturerInfo.email}/>
                        </div>
                        <div className="input-block">
                            <label htmlFor="phone">Enter phone:</label>
                            <input id="phone" type="text" onChange={handleInputChange} value={isStudent ? studentInfo.phone : lecturerInfo.phone}/>
                        </div>
                        <div className="input-block">
                            <label htmlFor="birthday">Birthday:</label>
                            <input id="birthday" type="date" onChange={handleInputChange} value={isStudent ? studentInfo.birthday : lecturerInfo.birthday}/>
                        </div>
                        { isLecturer && <div className="input-block">
                            <label htmlFor="academic-degree-selector">Select the academic degree</label>
                            <select id="academic-degree-selector" onChange={handleSelectChange}>
                                { academicDegreeList.map((value) => <option key={value.id} value={value.id}>{value.name}</option>)}
                            </select>
                        </div> }
                        <button onClick={handleRegistrate}>Registrate</button>
                    </form>
                </div>
            </ContentHolder>
        </PageContainer>
    );
}

export default RegistrationPage;