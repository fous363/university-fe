import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useParams } from 'react-router-dom';
import ConfirmPopup from '../../../component/confirm-popup/ConfirmPopup';
import GroupResponse from '../../../type/GroupResponse';
import LecturerResponse from '../../../type/LecturerResponse';
import StudentResponse from '../../../type/StudentResponse';
import User from '../../../type/User';
import constainsRole, { axiosInstance } from '../../../utils';
import GroupAssign from '../assign/GroupAssign';
import './GroupSingle.scss';

interface LecturerModel {
    id: number,
    name: string
}

interface GroupUpdateModel {
    id: number,
    name: string,
    responsiblePersonId?: number | null
}

export default function GroupSingle(props: any) {
    const params: any = useParams();
    const groupId: number = Number.parseInt(params.groupId);
    const history = useHistory();

    const [group, setGroup] = useState({ id: 0, name: '', responsiblePersonId: 0});
    const [isGroupLoaded, setGroupLoaded] = useState(false);
    const [responsiblePerson, setResponsiblePerson] = useState({ id: 0, name: ''});
    const [showStudents, setShowStudents] = useState(false);
    const [isStudentsLoaded, setStudentsLoaded] = useState(false);
    const [students, setStudents] = useState([{ id: 0, name: ''}]);
    const [deleteMode, setDeleteMode] = useState(false);
    const [updateMode, setUpdateMode] = useState(false);
    const [lecturers, setLecturers] = useState<LecturerModel[]>([{ id: 0, name: ''}]);
    const [groupUpdate, setGroupUpdate] = useState<GroupUpdateModel>({ id: 0, name: ''});
    const [assignMode, setAssignMode] = useState(false);
    const [unassignMode, setUnassignMode] = useState(false);
    const [unassignedStudent, setUnassignedStudent] = useState({id: 0, name: ''});

    const storedUser: any = localStorage.getItem('storedUser'); // todo: think about other solution for localStorage
    if (!storedUser) {
        throw new Error('You are not authenticated');
    }
    const userInfo: User = JSON.parse(storedUser);
    
    const isAdmin: boolean = constainsRole(userInfo.roles, 'admin');

    useEffect(() => {
        if (!isGroupLoaded) {
            axiosInstance.get(`api/group/${groupId}`)
            .then(rsp => {
                const groupResponse: GroupResponse = rsp.data;
                console.log(groupResponse);
                if (groupResponse.responsiblePersonId !== null && groupResponse.responsiblePersonId! > 0) {
                    axiosInstance.get(`api/lecturer/${groupResponse.responsiblePersonId}`)
                    .then(rsp => {
                        const responsiblePersonResponse: LecturerResponse = rsp.data;
                        console.log(responsiblePersonResponse);
                        setResponsiblePerson({ id: responsiblePersonResponse.id, name: responsiblePersonResponse.fullName });
                    }).catch(err => console.error(err));
                }
                setGroup({ id: groupResponse.id, name: groupResponse.name, responsiblePersonId: groupResponse.responsiblePersonId! });
                setGroupLoaded(true);
            }).catch(err => console.error(err));
        }
    }, [isGroupLoaded, groupId]);

    const handleStudentBurgerClick = () => {
        const isDisplay: boolean = !showStudents;
        setShowStudents(isDisplay);
        if (isDisplay) {
            axiosInstance.get(`api/group/${group.id}/students/id`).then(rsp => {
                const studentIds: number[] = rsp.data;
                if (studentIds.length > 0) {
                    axiosInstance.get(`api/student?ids=${studentIds.join(',')}`)
                    .then(rsp => {
                        const studentsResponse: StudentResponse[] = rsp.data;
                        setStudents(studentsResponse.map(student => { return { id: student.id, name: student.fullName } }));
                    }).catch(err => console.error(err));
                } else {
                    setStudents([]);
                }
                setStudentsLoaded(true);
            }).catch(err => console.error(err));
        } else {
            setStudentsLoaded(false);
        }
    }

    const handleDelete = () => {
        axiosInstance.delete(`api/group/${group.id}`)
        .then(rsp => {
            history.push('/group');
        }).catch(err => console.error(err));
    }

    const handleUpdateBtnClick = () => {
        setUpdateMode(true); 
        setGroupUpdate({ id: group.id, name: group.name, responsiblePersonId: group.responsiblePersonId!});
        axiosInstance.get('api/lecturer').then(rsp => {
            const lecturersResponse: LecturerResponse[] = rsp.data;
            setLecturers(lecturersResponse.map(lecturer => { return {id: lecturer.id, name: lecturer.fullName}; }));
        }).catch(err => console.error(err));
    }

    const handleUpdate = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        if (groupUpdate.responsiblePersonId === 0) {
            setGroupUpdate({...groupUpdate, responsiblePersonId: null});
        }
        axiosInstance.put('api/group', groupUpdate)
        .then(rsp => {
            setGroupLoaded(false);
            setUpdateMode(false);
        }).catch(err => console.error(err));
    }

    const handleUnassignBtnClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setUnassignMode(true);
        const studentId: number = students.findIndex(student => student.id === Number.parseInt(event.currentTarget.value));
        setUnassignedStudent(students[studentId]);
    }

    const handleUnassign = () => {
        axiosInstance.post(`api/group/${group.id}/unassign/student/${unassignedStudent.id}`)
        .then(rsp => {
            setStudents(students.filter(student => student.id !== unassignedStudent.id));
            setUnassignMode(false);
            setUnassignedStudent({id: 0, name: ''});
        }).catch(err => console.error(err));
    }

    return (
        <div className="group-single-container">
            <div className="header">
                <i className="material-icons" onClick={() => history.goBack()}>&#xe5c4;</i>
                <h1>Group</h1>
                    { !updateMode && isAdmin &&
                    <div className="btn-group">
                        <button className="update-btn" onClick={handleUpdateBtnClick}>UPDATE</button>
                        <button className="delete-btn" value={group.id} onClick={() => setDeleteMode(true)}>DELETE</button>
                        <button className="assign-btn" onClick={() => setAssignMode(true)}>ASSIGN STUDENTS</button>
                        { deleteMode && 
                            <ConfirmPopup 
                            text="Are you sure you want to delete this group?" 
                            performHandler={handleDelete} 
                            closeHandler={() => setDeleteMode(false)}/>
                        }
                        { assignMode && 
                            <GroupAssign
                            closeHandler={() => setAssignMode(false)}
                            groupId={group.id}/>
                        }
                        { unassignMode &&
                            <ConfirmPopup
                            text={`Are you sure you want to unassign student '${unassignedStudent.name}' from this group?`}
                            performHandler={handleUnassign}
                            closeHandler={() => setUnassignMode(false)}/>
                        }
                    </div>
                    }
            </div>
            { !updateMode &&
                <div className="group-info-container">
                    <div className="group-name-container">
                        <h3>Name:</h3>
                        <span>{group.name}</span>
                    </div>
                    <div className="group-responsible-person-container">
                        { responsiblePerson.id > 0 && 
                        <h5>Responsible person for this group: {responsiblePerson.name}</h5>
                        }
                        { responsiblePerson.id === 0 && 
                        <h5>No responsible person for this group</h5>
                        }
                    </div>
                    <div className="group-students-container">
                        <div className="group-students-burger" onClick={handleStudentBurgerClick}>
                                <h3>Students</h3>
                                { !showStudents && <i className="material-icons">&#xe5cf;</i> }
                                { showStudents && <i className="material-icons">&#xe5ce;</i>}
                            </div>
                            { showStudents && !isStudentsLoaded && 
                            <div className="loading-placeholder">
                                <h1>Loading...</h1>
                            </div>
                            }
                            { showStudents && isStudentsLoaded && 
                            <ul className="students-list">
                                { students.map(student =>
                                    <li key={student.id} className="student-element">
                                        <span>{student.name}</span>
                                        <button className="unassign-btn" value={student.id} onClick={handleUnassignBtnClick}>UNASSIGN</button>
                                    </li>
                                )}
                            </ul> }
                    </div>
                </div>
            }
            { updateMode && 
                <form className="update-group-form">
                    <label htmlFor="group-name">Name:</label>
                    <input id="group-name" name="name" value={groupUpdate.name} onChange={(event) => setGroupUpdate({ ...groupUpdate, name: event.target.value})}/>
                    <div className="responsible-person-select">
                        <label htmlFor="person-select">Chosse responsible person for this group:</label>
                        <select id="person-select" 
                        onChange={(event) => setGroupUpdate({...groupUpdate, responsiblePersonId: Number.parseInt(event.currentTarget.value)}) }
                        defaultValue={groupUpdate.responsiblePersonId!}> {/* FIX ME, wrong person on first try */}
                            <option value={0}>None</option>
                            {lecturers.map(lecturer => 
                                <option key={lecturer.id} value={lecturer.id}>{lecturer.name}</option>
                            )
                            }
                        </select>
                    </div>
                    <div className="btn-group">
                        <button className="save-btn" onClick={handleUpdate}>SAVE</button>
                        <button className="cancel-btn" onClick={() => setUpdateMode(false)}>CANCEL</button>
                    </div>
                </form>
            }
        </div>
    );
}