import React, { useEffect, useState } from "react";
import StudentResponse from "../../../type/StudentResponse";
import { axiosInstance } from "../../../utils";
import './GroupAssign.scss';

interface StudentModel {
    id: number,
    name: string
}

export default function GroupAssign(props: any) {
    const groupId: number = Number.parseInt(props.groupId);

    const [isLoaded, setLoaded] = useState(false);
    const [unassignedStudents, setUnassignedStudents] = useState([{ id: 0, name: ''}]);
    const [assignedStudents, setAssignedStudents] = useState<StudentModel[] | []>([]);

    useEffect(() => {
        if (!isLoaded) {
            axiosInstance.get(`api/group/students/id`)
            .then(rsp => {
                const studentsId: number[] = rsp.data;
                axiosInstance.get(`api/student?exclude-ids=${studentsId.join(',')}`)
                .then(rsp => {
                    const students: StudentResponse[] = rsp.data;
                    setUnassignedStudents(students.map(student => { return {id: student.id, name: student.fullName} }));
                    setLoaded(true);
                }).catch(err => console.error(err));
            }).catch(err => console.error(err));
        }
    });

    const handleSelection = (event: any) => {
        let updatedAssignedStudents: StudentModel[] | [] = [];
        const selectedId: number = Number.parseInt(event.target.value);
        if (assignedStudents.some(student => student.id === selectedId)) {
            updatedAssignedStudents = assignedStudents.filter(student => student.id !== selectedId);
        } else {
            const index: number = unassignedStudents.findIndex(student => student.id === selectedId);
            updatedAssignedStudents = [...assignedStudents, unassignedStudents[index]];
        }
        setAssignedStudents(updatedAssignedStudents);
    }

    const handleAssign = () => {
        axiosInstance.post(`api/group/${groupId}/assign/student/list`, assignedStudents)
        .then(rsp => {
            props.closeHandler();
        }).catch(err => console.error(err));
    }

    return (
        <div className="assign-modal">
            <div className="modal-content">
                <div className="header">
                    <h3>Assign students to this group</h3>
                    <i className="material-icons" onClick={() => props.closeHandler()}>&#xe5cd;</i>
                </div>
                {!isLoaded && 
                <h1>Loading...</h1>}
                {isLoaded && 
                <ul className="unassign-student-list">
                    { unassignedStudents.map(student => {
                    if (assignedStudents.some(assignedStudent => assignedStudent.id === student.id)) {
                        return (
                            <li className="student-element">                                        
                                <input type="checkbox" onClick={handleSelection} value={student.id} checked/>
                                <span>{student.name}</span>
                            </li>
                        );
                    }
                    return (
                    <li className="student-element">                                        
                        <input type="checkbox" onClick={handleSelection} value={student.id}/>
                        <span>{student.name}</span>
                    </li>
                    );
                    })}
                </ul>}
                { assignedStudents.length === 0 && 
                <button className='assign-student-btn disabled' disabled>ASSIGN</button>
                }
                { assignedStudents.length > 0 && 
                <button className='assign-student-btn' onClick={handleAssign}>ASSIGN</button>
                }
            </div>
        </div>
    );
}