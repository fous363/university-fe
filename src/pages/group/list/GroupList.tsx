import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router';
import { Link } from 'react-router-dom';
import GroupResponse from '../../../type/GroupResponse';
import { axiosInstance } from '../../../utils';
import './GroupList.scss';

export default function GroupList() {
    let match = useRouteMatch();
    const [isLoaded, setLoaded] = useState(false);
    const [groups, setGroups] = useState([{ id: 0, name: '' }]);
    const [createMode, setCreateMode] = useState(false);
    const [groupCreate, setGroupCreate] = useState({ name: '' });

    useEffect(() => {
        if (!isLoaded) {
            axiosInstance.get('api/group')
            .then(rsp => {
                const groupsResponse: GroupResponse[] = rsp.data;
                setGroups(groupsResponse);
                setLoaded(true);
            }).catch(err => console.error(err));
        }
    }, [isLoaded]);

    const handleCreate = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        axiosInstance.post('api/group', groupCreate)
        .then(rsp => {
            setLoaded(false);
            setCreateMode(false);
        }).catch(err => console.error(err));
    }

    const handleCanceling = () => {
        setGroupCreate({ name: ''});
        setCreateMode(false);
    }

    if (createMode) {
        return (       
            <div className="create-group-container">
                <h3>Create new group</h3>
                <form className="create-group-form">
                    <label htmlFor="name">Name:</label>
                    <input type="text" name="name" id="group-name" onChange={(event) => setGroupCreate({ name: event.target.value})}/>
                    <div className="btn-group">
                        <button className="create-btn" onClick={handleCreate}>CREATE</button>
                        <button className="cancel-btn" onClick={handleCanceling}>CANCEL</button>
                    </div>
                </form>
            </div>
        );
    } else {
        return (
            <div className="group-list-container">
                <div className="header">
                    <h1>Groups</h1>
                    <button className="create-new-btn" onClick={() => { setCreateMode(true); }}>CREATE NEW</button>
                </div>
                <ul className="group-list">
                    { groups.map(group => 
                        <li className="group-element">
                            <Link className="group-element-link" to={`${match.path}/${group.id}`}>{group.name}</Link>
                        </li>
                    ) }
                </ul>
            </div>
        );
    }
}