import React from 'react';
import { BrowserRouter, Route, useRouteMatch } from 'react-router-dom';
import ContentHolder from '../../component/content/ContentHolder';
import Navbar from '../../component/navbar/Navbar';
import PageContainer from '../../component/page/PageContainer';
import GroupList from './list/GroupList';
import GroupSingle from './single/GroupSingle';

export default function GroupPage() {
    let match = useRouteMatch();

    return (
        <PageContainer>
            <Navbar currentPage="group"/>
            <ContentHolder>
                <BrowserRouter>
                    <Route path={`${match.path}/:groupId`} component={GroupSingle}/>
                    <Route path={match.path} exact component={GroupList}/>
                </BrowserRouter>
            </ContentHolder>
        </PageContainer>
    );
}