import React, { useState } from 'react';
import './LoginPage.scss';
import '../../component/error/ErrorHeader';
import { Link, useHistory } from 'react-router-dom';
import { ErrorResponse } from '../../type/ErrorResponse';
import User from '../../type/User';
import { axiosInstance } from '../../utils';

function LoginPage() {
    const [userInfo, setUserInfo] = useState({username: '', password: ''});
    const [errorText, setErrorText] = useState('');
    const history = useHistory();

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        switch (event.target.id) {
            case 'username':
                setUserInfo({...userInfo, username: event.target.value});
                break;
            case 'password':
                setUserInfo({...userInfo, password: event.target.value});
                break;
            default:
                break;
        }
    }

    const handleSubmit = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();
        
        if (!(userInfo.username || userInfo.password)) {
            setErrorText('Enter the username and password');
            return;
        }

        if (!userInfo.username) {
            setErrorText('Enter the username');
            return;
        }

        if (!userInfo.password) {
            setErrorText('Enter the password');
            return;
        }

        axiosInstance.post('api/user/auth', userInfo)
        .then((rsp) => {
            const user: User = rsp.data;
            console.log(user);
            localStorage.setItem('storedUser', JSON.stringify(user));
            history.push('/profile');
        })
        .catch((err) => {
            console.error(err);
            const errorResponse: ErrorResponse = err.response.data;
            if (errorResponse.message !== '') {
                setErrorText(errorResponse.message);
            } else {
                setErrorText(errorResponse.error);
            }
        });
    }
    
    if (errorText !== '') {
        throw new Error(errorText);
    }
    return (
    <div className="container">
        <div className="form">
            <h1>Login</h1>
            <form action="#" method="POST">
                <div className="input">
                    <h4>Username</h4>
                    <input type="text" name="username" id="username" onChange={handleInputChange}/>
                </div>
                <div className="input">
                    <div className="input-header">
                        <h4>Password</h4>
                    </div>
                    <input type="password" name="password" id="password" onChange={handleInputChange}/>
                </div>
                <div className="input input-checkbox">
                    <input type="checkbox" name="remember-me" id="remember-me" onChange={handleInputChange}/>
                    <h4>Remember Me</h4>
                </div>
                <button type="submit" onClick={handleSubmit}>Login</button>
            </form>
        </div>
        <Link to="/registration" className="signup-btn">Create an account</Link>
    </div>
    );
}

export default LoginPage;