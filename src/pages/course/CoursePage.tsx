import React from 'react';
import { useRouteMatch } from 'react-router';
import { BrowserRouter, Route } from 'react-router-dom';
import ContentHolder from '../../component/content/ContentHolder';
import Navbar from '../../component/navbar/Navbar';
import PageContainer from '../../component/page/PageContainer';
import CourseList from './list/CourseList';
import CourseSingle from './single/CourseSingle';

function CoursePage() {
    let match = useRouteMatch();

    return(
        <PageContainer>
            <Navbar currentPage="course"/>
            <ContentHolder>
                <BrowserRouter>
                    <Route path={`${match.path}/:courseId`} component={CourseSingle}/>
                    <Route path={match.path} exact component={CourseList}/>
                </BrowserRouter>
            </ContentHolder>
        </PageContainer>
    );
}

export default CoursePage;