import React, { useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { axiosInstance } from '../../../utils';
import CourseResponse from '../../../type/CourseResponse';
import { ErrorResponse } from '../../../type/ErrorResponse';
import './CourseList.scss';

export default function CourseList(props: any) {
    let match: any = useRouteMatch();
    const [isLoading, setLoading] = useState(true);
    const [courseList, setCourseList] = useState([{ id: 0, name: '' }]);
    const [error, setError] = useState('');
    const [createMode, setCreateMode] = useState(false);
    const [courseCreate, setCourseCreate] = useState({ name: '' });

    useEffect(() => {
        if (isLoading) {
            axiosInstance.get('api/course').then(rsp => {
                const couresListRsp: CourseResponse[] = rsp.data;
                setCourseList(couresListRsp);
                setLoading(false);
            }).catch(err => {
                const errorRsp: ErrorResponse = err.response.data;
                if (errorRsp.message !== '') {
                    setError(errorRsp.message);
                } else {
                    setError(errorRsp.error);
                }
                console.error(error);
            });
        }
    }, [isLoading, error]);

    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        switch (event.target.id) {
            case 'course-name':
                setCourseCreate({ name: event.target.value });
                break;
            default:
                break;
        }
    }

    const handleCreate = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        axiosInstance.post('api/course', courseCreate).then(rsp => {
            console.log(rsp.data);

            setLoading(true);
            setCreateMode(false);
        }).catch(err => {
            const errorRsp: ErrorResponse = err.response.data;
            if (errorRsp.message !== '') {
                setError(errorRsp.message);
            } else {
                setError(errorRsp.error);
            }
            console.error(error);
        });
    }

    const handleCanceling = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setLoading(true);
        setCreateMode(false);
    }

    if (createMode) {
        return (       
        <div className="create-course-container">
            <h3>Create new course</h3>
            <form className="create-course-form">
                <label htmlFor="name">Name:</label>
                <input type="text" name="name" id="course-name" onChange={handleOnChange}/>
                <div className="btn-container">
                    <button className="create-btn" onClick={handleCreate}>CREATE</button>
                    <button className="cancel-btn" onClick={handleCanceling}>CANCEL</button>
                </div>
            </form>
        </div>
        )
    } else {
        return (
        <div className="course-list-container">
            <div className="header">
                <h1>Courses</h1>
                <button className="create-new-btn" onClick={() => { setCreateMode(true); }}>CREATE NEW</button>
            </div>
            <ul className="course-list">
                { courseList.map(course => 
                <li key={course.id} className="course-element">
                    <Link className="course-element-link" to={`${match.path}/${course.id}`}>{course.name}</Link>
                </li>
                )
                }
            </ul> 
        </div> 
        )
    }
}