import React, { useEffect, useState } from "react";
import { axiosInstance } from "../../../utils";
import StudentResponse from '../../../type/StudentResponse';
import LecturerResponse from '../../../type/LecturerResponse';
import './CourseAssign.scss';

interface UserModel {
    id: number,
    name: string
}

export default function CourseAssign(props: any) {
    const isStudentAssign = props.isStudent === undefined ? false : props.isStudent;
    const courseId: number = Number.parseInt(props.courseId);
    const headerName = isStudentAssign === true ? 'Assign students to current course' : 'Assign lecturers to current course';
    const [unassignedUsers, setUnassignedUsers] = useState([{ id: 0, name: ''}]);
    const [assignedUsers, setAssignedUsers] = useState<UserModel[] | []>([]);
    const [isLoaded, setLoaded] = useState(false);

    useEffect(() => {
        if (!isLoaded) {
            if (isStudentAssign) {
                axiosInstance.get('api/student/unassigned/course/' + courseId)
                .then(rsp => {
                    const students: StudentResponse[] = rsp.data;
                    console.log(students);
                    setUnassignedUsers(students.map(student => { return {id: student.id, name: student.fullName} }));
                    setLoaded(true);
                }).catch(err => {
                    console.error(err);
                });
            } else {
                axiosInstance.get('api/lecturer/unassigned/course/' + courseId)
                .then(rsp => {
                    const lecturers: LecturerResponse[] = rsp.data;
                    setUnassignedUsers(lecturers.map(lecturer => { return {id: lecturer.id, name: lecturer.fullName} }));
                    setLoaded(true);
                }).catch(err => {
                    console.error(err);
                });
            }
            
        }
    }, [isLoaded, isStudentAssign, courseId]);

    const handleUserSelection = (event: any) => {
        let updatedAssignedUsers: UserModel[] | [] = [];
        const selectedUserId: number = Number.parseInt(event.target.value);
        if (assignedUsers.some(user => user.id === selectedUserId)) {
            updatedAssignedUsers = assignedUsers.filter(user => user.id !== selectedUserId);
        } else {
            const userId: number = unassignedUsers.findIndex(user => user.id === selectedUserId);
            updatedAssignedUsers = [...assignedUsers, unassignedUsers[userId]];
        }
        setAssignedUsers(updatedAssignedUsers);
    }

    const handleClose = (event: any) => {
        props.closeHandler();
    }

    const handleAssign = (event: any) => {
        if (isStudentAssign) {
            axiosInstance.post('api/student/list/assign/course/' + courseId, assignedUsers)
            .then(rsp => {
                setUnassignedUsers(unassignedUsers.filter(user => assignedUsers.some(assignedUser => assignedUser.id !== user.id)));
                setAssignedUsers([]);
            }).catch(err => {
                console.error(err);
            });
        } else {
            axiosInstance.post('api/lecturer/assign/list/course/' + courseId, assignedUsers)
            .then(rsp => {
                setUnassignedUsers(unassignedUsers.filter(user => assignedUsers.some(assignedUser => assignedUser.id !== user.id)));
                setAssignedUsers([]);
            }).catch(err => {
                console.error(err);
            });
        }
    }

    return (
        <div className="assign-student-modal">
            <div className="modal-content">
                <div className="header">
                    <h3>{headerName}</h3>
                    <i className="material-icons" onClick={handleClose}>&#xe5cd;</i>
                </div>
                    {!isLoaded && 
                    <h1>Loading...</h1>}
                    {isLoaded && 
                    <ul className="unassign-student-list">
                        { unassignedUsers.map(user => {
                        if (assignedUsers.some(assignUser => assignUser.id === user.id)) {
                            return (
                                <li className="student-element">                                        
                                    <input type="checkbox" onClick={handleUserSelection} value={user.id} checked/>
                                    <span>{user.name}</span>
                                </li>
                            );
                        }
                        return (
                        <li className="student-element">                                        
                            <input type="checkbox" onClick={handleUserSelection} value={user.id}/>
                            <span>{user.name}</span>
                        </li>
                        );
                        })}
                    </ul>}
                    { assignedUsers.length === 0 && 
                    <button className='assign-btn disabled' disabled>ASSIGN</button>
                    }
                    { assignedUsers.length > 0 && 
                    <button className='assign-btn' onClick={handleAssign}>ASSIGN</button>
                    }
            </div>
        </div>
    );
}