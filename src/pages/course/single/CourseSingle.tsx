import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import { axiosInstance} from '../../../utils';
import { ErrorResponse } from '../../../type/ErrorResponse';
import './CourseSingle.scss';
import CourseAssign from '../assign-modal/CourseAssign';
import ConfirmPopup from '../../../component/confirm-popup/ConfirmPopup';
import StudentResponse from '../../../type/StudentResponse';
import CourseResponse from '../../../type/CourseResponse';
import LecturerResponse from '../../../type/LecturerResponse';

export default function CourseSingle(props: any) {
    const params: any = useParams();
    const courseId: number = Number.parseInt(params.courseId);
    const history: any = useHistory();
    const courseInitState: { id: number, name: string } = {id: 0, name: ''};

    const [isLoading, setLoading] = useState(true);
    const [course, setCourse] = useState(courseInitState);
    const [courseUpdate, setCourseUpdate] = useState(courseInitState);
    const [updateMode, setUpdateMode] = useState(false);
    const [deleteMode, setDeleteMode] = useState(false);
    const [error, setError] = useState('');
    const [students, setStudents] = useState([{id: 0, name: ''}]);
    const [showStudents, setShowStudents] = useState(false);
    const [isStudentsLoaded, setStudentsLoaded] = useState(false);
    const [assignMode, setAssignMode] = useState(false);
    const [assignStudents, setAssignStudents] = useState(false);
    const [unassignMode, setUnassignMode] = useState(false);
    const [unassignUser, setUnassignUser] = useState({ id: 0, name: ''});
    const [assignedLecturers, setAssignedLecturers] = useState([{ id: 0, name: ''}]);
    const [showLecturers, setShowLecturers] = useState(false);
    const [isLecturersLoaded, setLecturerLoaded] = useState(false);

    useEffect(() => {
        if (isLoading) {
            axiosInstance.get(`api/course/${courseId}`).then(rsp => {
                const courseRsp: CourseResponse = rsp.data;
                setCourse(courseRsp);
                setLoading(false);
            }).catch(err => console.log(err));
        }
    }, [isLoading, courseId]);

    const handleDeleteClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setDeleteMode(true);
    }

    const handleDeleteCancel = () => {
        setDeleteMode(false);
    }

    const handleDelete = () => {
        
        axiosInstance.delete(`api/course/${courseId}`).then(rsp => {
            history.push('/course');
        }).catch(err => {
            const errorRsp: ErrorResponse = err.response.data;
            if (errorRsp.message !== '') {
                setError(errorRsp.message);
            } else {
                setError(errorRsp.error);
            }
            console.error(error);
        });
    }

    const handleUpdateClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setUpdateMode(true);
        setCourseUpdate(course);
    }

    const handleUpdateOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCourseUpdate({ ...courseUpdate, name: event.target.value });
    }

    const handleUpdateCancel = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setUpdateMode(false);
        setCourseUpdate(courseInitState);
    }

    const handleUpdateSave = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        axiosInstance.put('api/course', courseUpdate)
        .then(rsp => {
            setUpdateMode(false);
            setCourse(courseUpdate);
            setCourseUpdate(courseInitState);
        })
        .catch(err => {
            const errorRsp: ErrorResponse = err.response.data;
            if (errorRsp.message !== '') {
                setError(errorRsp.message);
            } else {
                setError(errorRsp.error);
            }
            console.error(error);
        });
    }

    const handleStudentBurgerClick = () => {
        const isDisplay: boolean = !showStudents;
        setShowStudents(isDisplay);
        if (isDisplay) {
            axiosInstance.get(`api/student/assigned/course/${courseId}`).then((rsp) => {
                const students: StudentResponse[] = rsp.data;
                setStudents(students.map(student => { return { id: student.id, name: student.fullName } }))
                setStudentsLoaded(true);
            }).catch(err => {
                const errorRsp: ErrorResponse = err.response.data;
                if (errorRsp.message !== '') {
                    setError(errorRsp.message);
                } else {
                    setError(errorRsp.error);
                }
                console.error(error);
            });
        } else {
            setStudentsLoaded(false);
        }
    }

    const handleAssignStudentClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setAssignMode(true);
        setAssignStudents(true);
    }

    const handleAssignClose = () => {
        setAssignMode(false);
    }

    const handleUnassignStudentClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        event.preventDefault();

        setUnassignMode(true);
        const studentId: number = students.findIndex(student => student.id === Number.parseInt(event.currentTarget.value));
        setUnassignUser(students[studentId]);
    }

    const handleUnassignStudent = () => {
        axiosInstance.post(`api/student/${unassignUser.id}/unassign/course/${courseId}`)
        .then(rsp => {
            setStudents(students.filter(student => student.id !== unassignUser.id));
        }).catch(err => console.error(err));
    }

    const handleUnassignStudentClose = () => {
        setUnassignMode(false);
        setUnassignUser({ id: 0, name: ''});
    }

    const handleLecturerBurgerClick = () => {
        const isDisplay: boolean = !showLecturers;
        setShowLecturers(isDisplay);
        if (isDisplay) {
            axiosInstance.get(`api/lecturer/assigned/course/${courseId}`).then((rsp) => {
                const lecturers: LecturerResponse[] = rsp.data;
                setAssignedLecturers(lecturers.map(lecturer => { return { id: lecturer.id, name: lecturer.fullName } }));
                setLecturerLoaded(true);
            }).catch(err => {
                const errorRsp: ErrorResponse = err.response.data;
                if (errorRsp.message !== '') {
                    setError(errorRsp.message);
                } else {
                    setError(errorRsp.error);
                }
                console.error(error);
            });
        } else {
            setLecturerLoaded(false);
        }
    }

    const handleUnassignLecturerClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        setUnassignMode(true);
        const lecturerId: number = assignedLecturers.findIndex(lecturer => lecturer.id === Number.parseInt(event.currentTarget.value));
        setUnassignUser(assignedLecturers[lecturerId]);
    }

    const handleUnassignLecturerClose = () => {
        setUnassignMode(false);
        setUnassignUser({ id: 0, name: '' });
    }

    const handleUnassignLecturer = () => {
        axiosInstance.post(`api/lecturer/${unassignUser.id}/unassign/course/${courseId}`)
        .then(rsp => {
            setAssignedLecturers(assignedLecturers.filter(lecturer => lecturer.id !== unassignUser.id));
        }).catch(err => console.error(err));
    }

    const handleAssignLecturerClick = (event: React.MouseEvent<HTMLButtonElement, globalThis.MouseEvent>) => {
        setAssignMode(true);
        setAssignStudents(false);
    }

    return (
        <div className="course-single-container">
            <div className="header">
                <i className="material-icons" onClick={() => history.push('/course')}>&#xe5c4;</i>
                <h1>Course</h1>
                { !updateMode && 
                <div className="btn-group">
                    <button className="update-btn" onClick={handleUpdateClick}>UPDATE</button>
                    <button className="delete-btn" value={course.id} onClick={handleDeleteClick}>DELETE</button>
                    <button className="assign-student-btn" onClick={handleAssignStudentClick}>ASSIGN STUDENT</button>
                    <button className="assign-lecturer-btn" onClick={handleAssignLecturerClick}>ASSIGN LECTURER</button>
                    { deleteMode && 
                        <ConfirmPopup 
                        text="Are you sure you want to delete this coures?" 
                        performHandler={handleDelete} 
                        closeHandler={handleDeleteCancel}/>
                    }
                    { assignMode && 
                        <CourseAssign 
                        isStudent={assignStudents} 
                        courseId={courseId} 
                        closeHandler={handleAssignClose}/>
                    }
                    { unassignMode && showStudents && 
                        <ConfirmPopup 
                        text={`Are you sure you want to unassign student "${unassignUser.name}" from this coures?`}
                        closeHandler={handleUnassignStudentClose}
                        performHandler={handleUnassignStudent}/>
                    }
                    { unassignMode && showLecturers && 
                        <ConfirmPopup text={`Are you sure you want to unassign lecturer "${unassignUser.name}" from this coures?`}
                        closeHandler={handleUnassignLecturerClose}
                        performHandler={handleUnassignLecturer}/>
                    }
                </div>
                }
            </div>
            { !updateMode && 
                <div className="course-info-container">
                    <div className="course-name-container">
                        <h3>Name:</h3>
                        <span>{course.name}</span>
                    </div>
                    <div className="course-student-container">
                        <div className="course-student-burger" onClick={handleStudentBurgerClick}>
                            <h3>Assigned students</h3>
                            { !showStudents && <i className="material-icons">&#xe5cf;</i> }
                            { showStudents && <i className="material-icons">&#xe5ce;</i>}
                        </div>
                        { showStudents && !isStudentsLoaded && 
                        <div className="loading-placeholder">
                            <h1>Loading...</h1>
                        </div>
                        }
                        { showStudents && isStudentsLoaded && 
                        <ul className="students-list">
                            { students.map(student =>
                                <li key={student.id} className="student-element">
                                    <span>{student.name}</span>
                                    <button className="unassign-btn" value={student.id} onClick={handleUnassignStudentClick}>UNASSIGN</button>
                                </li>
                            )}
                        </ul> }
                    </div>
                    <div className="course-lecturer-container">
                        <div className="course-lecturer-burger" onClick={handleLecturerBurgerClick}>
                            <h3>Assigned lecturers</h3>
                            { !showLecturers && <i className="material-icons">&#xe5cf;</i> }
                            { showLecturers && <i className="material-icons">&#xe5ce;</i>}
                        </div>
                        { showLecturers && !isLecturersLoaded && 
                        <div className="loading-placeholder">
                            <h1>Loading...</h1>
                        </div>
                        }
                        { showLecturers && isLecturersLoaded && 
                        <ul className="lecturer-list">
                            { assignedLecturers.map(lecturer =>
                                <li key={lecturer.id} className="lecturer-element">
                                    <span>{lecturer.name}</span>
                                    <button className="unassign-btn" value={lecturer.id} onClick={handleUnassignLecturerClick}>UNASSIGN</button>
                                </li>
                            )}
                        </ul> }
                    </div>
                </div>
            }
            { updateMode && 
                <form className="update-course-form">
                    <label htmlFor="course-name">Name:</label>
                    <input id="course-name" value={courseUpdate.name} onChange={handleUpdateOnChange}/>
                    <div className="btn-group">
                        <button className="save-btn" onClick={handleUpdateSave}>SAVE</button>
                        <button className="cancel-btn" onClick={handleUpdateCancel}>CANCEL</button>
                    </div>
                </form>
            }
        </div>
    );
}