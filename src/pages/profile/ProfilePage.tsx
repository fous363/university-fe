import React, { useEffect, useState } from "react";
import ContentHolder from '../../component/content/ContentHolder';
import './ProfilePage.scss';
import Navbar from '../../component/navbar/Navbar';
import PageContainer from '../../component/page/PageContainer';
import User from '../../type/User';
import { ErrorResponse } from "../../type/ErrorResponse";
import constainsRole, 
{ axiosInstance } from "../../utils";
import StudentResponse from '../../type/StudentResponse';
import LecturerResponse from '../../type/LecturerResponse';
import CourseResponse from "../../type/CourseResponse";
import { Link } from "react-router-dom";
import GroupResponse from "../../type/GroupResponse";

function ProfilePage() {
    const [error, setError] = useState('');
    const [isLoading, setLoading] = useState(true);
    const [student, setStudent] = useState({ id: 0, fullName: '', phone: '', birthday: null, email: ''});
    const [lecturer, setLecturer] = useState({ id: 0, fullName: '', phone: '', birthday: null, email: '', academicDegree: ''});
    const [isCoursesLoaded, setCoursesLoaded] = useState(false);
    const [showCourses, setShowCourses] = useState(false);
    const [courses, setCourses] = useState([{ id: 0, name: ''}]);
    const [group, setGroup] = useState({ id: 0, name: ''});

    const storedUser: any = localStorage.getItem('storedUser'); // todo: think about other solution for localStorage
    if (!storedUser) {
        throw new Error('You are not authenticated');
    }
    const userInfo: User = JSON.parse(storedUser);
    
    const isStudent: boolean = constainsRole(userInfo.roles, 'student');
    const isLecturer: boolean = constainsRole(userInfo.roles, 'lecturer');
    if (!(isLecturer || isStudent)) {
        throw new Error('You are not authorized.');
    }

    useEffect(() => {
        async function fetchData() {
            let groupRequestUrl: string = '';
            if (isStudent) {
                const studentResponse: StudentResponse = (await axiosInstance.get('api/student/user/' + userInfo.id)).data;
                setStudent({ id: studentResponse.id,
                        fullName: studentResponse.fullName, 
                        phone: studentResponse.phone!, 
                        birthday: studentResponse.birthday, 
                        email: studentResponse.email!});
                groupRequestUrl = `api/group/student/${studentResponse.id}`;
            }
            if (isLecturer) {
                const lecturerResponse: LecturerResponse = (await axiosInstance.get('api/lecturer/user/' + userInfo.id)).data;
                const academicDegree: any = (await axiosInstance.get('api/academicDegree/' + lecturerResponse.academicDegreeId)).data;
                setLecturer({ id: lecturerResponse.id,
                    fullName: lecturerResponse.fullName,
                    phone: lecturerResponse.phone!,
                    birthday: lecturerResponse.birthday,
                    email: lecturerResponse.email!,
                    academicDegree: academicDegree.name
                });
                groupRequestUrl = `api/group/responsiblePerson/${lecturerResponse.id}`;
            }
            const groupResponse: GroupResponse = (await axiosInstance.get("" + groupRequestUrl)).data;
            setGroup({ id: groupResponse.id, name: groupResponse.name });

        }

        if (isLoading) {
            try {
                fetchData();
            setLoading(false);

            } catch (e) {
                const errorRsp: ErrorResponse = e.response.data;
                if (errorRsp.message !== '') {
                    setError(errorRsp.message);
                } else {
                    setError(errorRsp.error);
                }
            }
        }
    }, [isLoading, isLecturer, isStudent, userInfo.id]);    
    
    const handleCoursesBurgerClick = () => {
        const isDisplay: boolean = !showCourses;
        setShowCourses(isDisplay);
        if (isDisplay) {
            const fetchCourses = (coursesId: number[]) => {
                axiosInstance.get(`api/course?ids=${coursesId.join(',')}`)
                .then(rsp => {
                    const coursesReponse: CourseResponse[] = rsp.data;
                    setCourses(coursesReponse.map(course => { return { id: course.id, name: course.name } }));
                }).catch(err => console.error(err));
            }
    
            if (isStudent) {
                axiosInstance.get(`api/student/${student.id}/courses/id`)
                .then(rsp => {
                    const coursesId: number [] = rsp.data;
                    if (coursesId.length > 0) {
                        fetchCourses(coursesId);
                    }
                }).catch(err => console.error(err));
            }
            if (isLecturer) {
                axiosInstance.get(`api/lecturer/${lecturer.id}/courses/id`)
                .then(rsp => {
                    const coursesId: number [] = rsp.data;
                    if (coursesId.length > 0) {
                        fetchCourses(coursesId);
                    }
                }).catch(err => console.error(err));
            }
            setCoursesLoaded(true);
        } else {
            setCoursesLoaded(false);
        }
    }

    // todo: think about other solution for useEffect
    if (error !== '') {
        throw new Error(error);
    }
    return (
        <PageContainer>
            <Navbar currentPage="profile"/>
            <ContentHolder isContentLoading={isLoading}>
                <div className="content">
                    <h1 className="page-name">Profile</h1>
                    <div className="profile-content">
                        { 
                            isStudent && 
                            <ul>
                                <li>Full Name: {student.fullName}</li>
                                <li>Phone: {student.phone}</li>
                                <li>Email: {student.email}</li>
                                <li>Birthday: {student.birthday}</li>
                                { group.id !== 0 &&
                                    <li>
                                    Group: <Link to={`/group/${group.id}`}>{group.name}</Link>
                                    </li>
                                }
                            </ul>
                        }
                        {
                            isLecturer && 
                            <ul>
                                <li>Full Name: {lecturer.fullName}</li>
                                <li>Phone: {lecturer.phone}</li>
                                <li>Email: {lecturer.email}</li>
                                <li>Birthday: {lecturer.birthday}</li>
                                <li>Academic degree: {lecturer.academicDegree}</li>
                                { group.id !== 0 &&
                                    <li>
                                    Group: <Link to={`/group/${group.id}`}>{group.name}</Link>
                                    </li>
                                }
                            </ul>
                        }
                        <div className="course-list-container">
                            <div className="course-burger" onClick={handleCoursesBurgerClick}>
                                <h3>Courses</h3>
                                { !showCourses && <i className="material-icons">&#xe5cf;</i> }
                                { showCourses && <i className="material-icons">&#xe5ce;</i>}
                            </div>
                            { showCourses && !isCoursesLoaded && 
                            <div className="loading-placeholder">
                                <h1>Loading...</h1>
                            </div>
                            }
                        { showCourses && isCoursesLoaded && 
                        <ul className="course-list">
                            { courses.map(course =>{
                                if (course.id !== 0) {
                                    return(
                                    <li key={course.id} className="course-element">
                                    {course.name}
                                    </li>);
                                }
                                return null;
                            })}
                        </ul> }
                        </div>
                    </div>
                </div>
            </ContentHolder>
        </PageContainer>
    );
}

export default ProfilePage;