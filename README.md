# University

## Boot

To start the application, the following environment variables must be specified:

<pre>
    <code>
REACT_APP_SERVICE_URL={server url}
    </code>
</pre>

## Additional information

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
